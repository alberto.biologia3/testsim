#Crear los directorios una vez antes de crear el bucle para no crearlos cada vez
cd $WD
mkdir -p out/fastqc
mkdir -p log/cutadapt
mkdir -p out/cutadapt
mkdir -p res/genome
mkdir -p res/genome/star_index
#Descargar el genoma
cd res/genome
wget -O ecoli.fasta.gz ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz
gunzip -k ecoli.fasta.gz
#Indexar el genoma (solo se tiene que hacer una vez por eso esta fuera del bucle)
cd $WD
STAR --runThreadN 4 --runMode genomeGenerate --genomeDir res/genome/star_index/ --genomeFastaFiles res/genome/ecoli.fasta --genomeSAindexNbases 9
#Analizar todas las muestras

for sid in $(ls data/*.fastq.gz | cut -d "_" -f1 | sed 's:data/::' | sort | uniq)
do
	bash scripts/analyse_sample.sh $sid 
done
#Hacer el multiqc
multiqc -o out/multiqc $WD 
